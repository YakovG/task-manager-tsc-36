package ru.goloshchapov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.endpoint.Session;


public final class DataXmlLoadJaxBCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "data-xml-jaxb-load";

    @NotNull public static final String DESCRIPTION = "Load data from XML (JAXB)";

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return NAME;
    }

    @Override
    public @NotNull String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[DATA XML LOAD]");
        endpointLocator.getAdminEndpoint().loadXmlJaxBData(session);
    }

}
