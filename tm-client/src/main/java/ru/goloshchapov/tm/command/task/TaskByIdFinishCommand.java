package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.endpoint.Task;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByIdFinishCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-finish-by-id";

    @NotNull public static final String DESCRIPTION = "Finish task by id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final Task task = endpointLocator.getTaskEndpoint().finishTaskById(session, taskId);
        if (task == null) throw new TaskNotFoundException();
    }
}
