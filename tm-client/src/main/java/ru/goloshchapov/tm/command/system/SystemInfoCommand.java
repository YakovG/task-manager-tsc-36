package ru.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.util.NumberUtil;

public final class SystemInfoCommand extends AbstractCommand {

    @NotNull public static final String ARGUMENT = "-i";

    @NotNull public static final String NAME = "info";

    @NotNull public static final String DESCRIPTION = "Show system info";

    @NotNull
    @Override
    public String arg() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        @NotNull final String freeMemoryFormat = NumberUtil.format(freeMemory);
        @NotNull final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        @NotNull final String maxMemoryFormat = NumberUtil.format(maxMemory);
        @NotNull final String maxMemoryValue = isMaxMemory ? "no limit" : maxMemoryFormat;
        final long totalMemory = Runtime.getRuntime().totalMemory();
        @NotNull final String totalMemoryFormat = NumberUtil.format(totalMemory);
        final long usedMemory = totalMemory - freeMemory;
        @NotNull final String usedMemoryFormat = NumberUtil.format(usedMemory);
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);
        System.out.println("Maximum memory (bytes): " + maxMemoryValue);
        System.out.println("Total memory available to JVM (bytes): " + totalMemoryFormat);
        System.out.println("Memory used by JVM (bytes): " + usedMemoryFormat);
    }
}
