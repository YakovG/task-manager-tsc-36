package ru.goloshchapov.tm.repository;

import org.junit.Assert;
import org.junit.Test;
import ru.goloshchapov.tm.model.Session;

public class SessionRepositoryTest {

    @Test
    public void testMerge() {
        final SessionRepository sessionRepository = new SessionRepository();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        final Session session = new Session();
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        session.setUserId("test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("test", session.getUserId());
        session.setSignature("sign");
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("sign", session.getSignature());
        Assert.assertNotNull(sessionRepository.merge(session));
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        final Session sessionById = sessionRepository.findOneById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
    }

    @Test
    public void testFindByUserId() {
        final SessionRepository sessionRepository = new SessionRepository();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        final Session session = new Session();
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        session.setUserId("test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("test", session.getUserId());
        session.setSignature("sign");
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("sign", session.getSignature());
        Assert.assertNotNull(sessionRepository.merge(session));
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        final Session sessionById = sessionRepository.findOneById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        Assert.assertNotNull(sessionRepository.findByUserId("test"));
        Assert.assertEquals(sessionById, sessionRepository.findByUserId("test"));
    }

    @Test
    public void testContains() {
        final SessionRepository sessionRepository = new SessionRepository();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        final Session session = new Session();
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        final String sessionId = session.getId();
        Assert.assertFalse(sessionRepository.contains(sessionId));
        Assert.assertNotNull(sessionRepository.merge(session));
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        Assert.assertTrue(sessionRepository.contains(sessionId));
    }

    @Test
    public void testExclude() {
        final SessionRepository sessionRepository = new SessionRepository();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        final Session session = new Session();
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        session.setUserId("test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("test", session.getUserId());
        session.setSignature("sign");
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("sign", session.getSignature());
        Assert.assertNotNull(sessionRepository.merge(session));
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        final Session sessionById = sessionRepository.findOneById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        sessionRepository.exclude(session);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }
}
