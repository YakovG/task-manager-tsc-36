package ru.goloshchapov.tm.repository;

import org.junit.Assert;
import org.junit.Test;
import ru.goloshchapov.tm.constant.RootUserConst;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.service.PropertyService;

import static ru.goloshchapov.tm.util.HashUtil.salt;

public class UserRepositoryTest {

    private final PropertyService propertyService = new PropertyService();

    @Test
    public void testCreate() {
        final UserRepository userRepository =
                new UserRepository(RootUserConst.ROOT_LOGIN, salt(propertyService, RootUserConst.ROOT_PASSWORD));
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        Assert.assertNotNull(userRepository.add(user));
        Assert.assertFalse(userRepository.findAll().isEmpty());
        final User userById = userRepository.findOneById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
    }

    @Test
    public void testRemove() {
        final UserRepository userRepository =
                new UserRepository(RootUserConst.ROOT_LOGIN, salt(propertyService, RootUserConst.ROOT_PASSWORD));
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        Assert.assertNotNull(userRepository.add(user));
        Assert.assertFalse(userRepository.findAll().isEmpty());
        final User userById = userRepository.findOneById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        Assert.assertNotNull(userRepository.removeOneById(userById.getId()));
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void testRemoveByLogin() {
        final UserRepository userRepository =
                new UserRepository(RootUserConst.ROOT_LOGIN, salt(propertyService, RootUserConst.ROOT_PASSWORD));
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        Assert.assertNotNull(userRepository.add(user));
        Assert.assertFalse(userRepository.findAll().isEmpty());
        final User userById = userRepository.findOneById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        Assert.assertNotNull(userById.getLogin());
        Assert.assertEquals("test",userById.getLogin());
        Assert.assertNotNull(userRepository.removeUserByLogin("test"));
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void testRemoveByEmail() {
        final UserRepository userRepository =
                new UserRepository(RootUserConst.ROOT_LOGIN, salt(propertyService, RootUserConst.ROOT_PASSWORD));
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        user.setEmail("test@test");
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("test@test", user.getEmail());
        Assert.assertNotNull(userRepository.add(user));
        Assert.assertFalse(userRepository.findAll().isEmpty());
        final User userById = userRepository.findOneById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        Assert.assertNotNull(userById.getEmail());
        Assert.assertEquals("test@test",userById.getEmail());
        Assert.assertNotNull(userRepository.removeUserByEmail("test@test"));
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void testClear() {
        final UserRepository userRepository =
                new UserRepository(RootUserConst.ROOT_LOGIN, salt(propertyService, RootUserConst.ROOT_PASSWORD));
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        final User user1 = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user1);
        Assert.assertNotNull(userRepository.add(user));
        Assert.assertNotNull(userRepository.add(user1));
        Assert.assertFalse(userRepository.findAll().isEmpty());
        userRepository.clear();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void testSize() {
        final UserRepository userRepository =
                new UserRepository(RootUserConst.ROOT_LOGIN, salt(propertyService, RootUserConst.ROOT_PASSWORD));
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        final User user1 = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user1);
        Assert.assertNotNull(userRepository.add(user));
        Assert.assertNotNull(userRepository.add(user1));
        Assert.assertFalse(userRepository.findAll().isEmpty());
        Assert.assertEquals(2,userRepository.size());
    }

    @Test
    public void testFind() {
        final UserRepository userRepository =
                new UserRepository(RootUserConst.ROOT_LOGIN, salt(propertyService, RootUserConst.ROOT_PASSWORD));
        Assert.assertTrue(userRepository.findAll().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        user.setEmail("test@test");
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("test@test", user.getEmail());
        Assert.assertNotNull(userRepository.add(user));
        Assert.assertFalse(userRepository.findAll().isEmpty());
        Assert.assertNotNull(userRepository.findUserByLogin("test"));
        Assert.assertEquals("test", userRepository.findUserByLogin("test").getLogin());
        Assert.assertNotNull(userRepository.findUserByEmail("test@test"));
        Assert.assertEquals("test@test", userRepository.findUserByEmail("test@test").getEmail());
    }
}
