package ru.goloshchapov.tm.repository;

import org.junit.Assert;
import org.junit.Test;
import ru.goloshchapov.tm.model.Project;

public class ProjectRepositoryTest {

    @Test
    public void testCreate() {
        final ProjectRepository projectRepository = new ProjectRepository();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        final Project project = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setName("PROJECT");
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("PROJECT", project.getName());
        Assert.assertNotNull(projectRepository.add(project));
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        final Project projectById = projectRepository.findOneById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(projectById.getId(), project.getId());
    }

    @Test
    public void testRemove() {
        final ProjectRepository projectRepository = new ProjectRepository();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        final Project project = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setName("PROJECT");
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("PROJECT", project.getName());
        Assert.assertNotNull(projectRepository.add(project));
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        final Project projectById = projectRepository.findOneById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(projectById.getId(), project.getId());
        Assert.assertNotNull(projectRepository.removeOneById(projectById.getId()));
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void testClear() {
        final ProjectRepository projectRepository = new ProjectRepository();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        final Project project = new Project();
        final Project project1 = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project1);
        Assert.assertNotNull(projectRepository.add(project));
        Assert.assertNotNull(projectRepository.add(project1));
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void testSize() {
        final ProjectRepository projectRepository = new ProjectRepository();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        final Project project = new Project();
        final Project project1 = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project1);
        Assert.assertNotNull(projectRepository.add(project));
        Assert.assertNotNull(projectRepository.add(project1));
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertEquals(2,projectRepository.size());
    }
}
