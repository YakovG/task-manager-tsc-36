package ru.goloshchapov.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.goloshchapov.tm.constant.RootUserConst;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.repository.UserRepository;

import static ru.goloshchapov.tm.util.HashUtil.salt;

public class UserServiceTest {

    private User user;

    private UserRepository userRepository;

    private PropertyService propertyService;

    @Before
    public void before() {
        propertyService = new PropertyService();
        user = new User();
        user.setId("ddd");
        user.setLogin("demo");
        user.setPasswordHash("demo");
        user.setEmail("demo@demo");
        final String password = RootUserConst.ROOT_PASSWORD;
        final String passwordHash = salt(propertyService, RootUserConst.ROOT_PASSWORD);
        userRepository =
                new UserRepository(RootUserConst.ROOT_LOGIN, salt(propertyService, RootUserConst.ROOT_PASSWORD));
        userRepository.add(user);
    }

    @Test
    public void testSetPassword() {
        final UserService userService = new UserService(userRepository, propertyService);
        Assert.assertNotNull(userService);
        Assert.assertNotNull(userService.findOneById("ddd"));
        Assert.assertNotNull(userService.findOneById("ddd").getPasswordHash());
        Assert.assertEquals("demo",userService.findOneById("ddd").getPasswordHash());
        Assert.assertNotNull(userService.setPassword("ddd", "password"));
        Assert.assertNotNull(userService.findOneById("ddd").getPasswordHash());
        Assert.assertEquals(salt(propertyService, "password"),userService.findOneById("ddd").getPasswordHash());
    }

    @Test
    public void testUpdateUser() {
        final UserService userService = new UserService(userRepository, propertyService);
        Assert.assertNotNull(userService);
        Assert.assertNotNull(userService.findOneById("ddd"));
        Assert.assertNull(userService.findOneById("ddd").getFirstname());
        Assert.assertNull(userService.findOneById("ddd").getLastname());
        Assert.assertNull(userService.findOneById("ddd").getMiddlename());
        Assert.assertNotNull(
                userService.updateUser("ddd","firstname", "lastname", "middlename" )
        );
        Assert.assertNotNull(userService.findOneById("ddd").getFirstname());
        Assert.assertNotNull(userService.findOneById("ddd").getLastname());
        Assert.assertNotNull(userService.findOneById("ddd").getMiddlename());
        Assert.assertEquals("firstname",userService.findOneById("ddd").getFirstname());
        Assert.assertEquals("lastname",userService.findOneById("ddd").getLastname());
        Assert.assertEquals("middlename",userService.findOneById("ddd").getMiddlename());
    }

    @Test
    public void testLockUnlockUserByLogin() {
        final UserService userService = new UserService(userRepository, propertyService);
        Assert.assertNotNull(userService);
        Assert.assertNotNull(userService.findOneById("ddd"));
        Assert.assertEquals("demo", userService.findOneById("ddd").getLogin());
        Assert.assertNotNull(userService.lockUserByLogin("demo"));
        Assert.assertNotNull(userService.findUserByLogin("demo"));
        Assert.assertEquals(userService.findOneById("ddd"), userService.findUserByLogin("demo"));
        Assert.assertTrue(userService.findUserByLogin("demo").isLocked());
        Assert.assertNotNull(userService.unlockUserByLogin("demo"));
        Assert.assertNotNull(userService.findOneById("ddd"));
        Assert.assertEquals("demo", userService.findOneById("ddd").getLogin());
        Assert.assertNotNull(userService.findUserByLogin("demo"));
        Assert.assertEquals(userService.findOneById("ddd"), userService.findUserByLogin("demo"));
        Assert.assertFalse(userService.findUserByLogin("demo").isLocked());
    }

    @Test
    public void testLockUnlockUserByEmail() {
        final UserService userService = new UserService(userRepository, propertyService);
        Assert.assertNotNull(userService);
        Assert.assertNotNull(userService.findOneById("ddd"));
        Assert.assertEquals("demo@demo", userService.findOneById("ddd").getEmail());
        Assert.assertNotNull(userService.lockUserByEmail("demo@demo"));
        Assert.assertNotNull(userService.findUserByEmail("demo@demo"));
        Assert.assertEquals(userService.findOneById("ddd"), userService.findUserByEmail("demo@demo"));
        Assert.assertTrue(userService.findUserByEmail("demo@demo").isLocked());
        Assert.assertNotNull(userService.unlockUserByEmail("demo@demo"));
        Assert.assertNotNull(userService.findOneById("ddd"));
        Assert.assertEquals("demo@demo", userService.findOneById("ddd").getEmail());
        Assert.assertNotNull(userService.findUserByEmail("demo@demo"));
        Assert.assertEquals(userService.findOneById("ddd"), userService.findUserByEmail("demo@demo"));
        Assert.assertFalse(userService.findUserByEmail("demo@demo").isLocked());
    }
}
