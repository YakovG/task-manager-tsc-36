package ru.goloshchapov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.constant.RootUserConst;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.repository.SessionRepository;
import ru.goloshchapov.tm.repository.UserRepository;


import static ru.goloshchapov.tm.util.HashUtil.salt;

public class SessionServiceTest implements ServiceLocator{

    private Session session;

    private SessionRepository sessionRepository;

    private ServiceLocator serviceLocator;

    private User user;

    private UserRepository userRepository;

    private UserService userService;

    private PropertyService propertyService;

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    @Before
    public void before() {
        serviceLocator = this;
        session = new Session();
        sessionRepository = new SessionRepository();
        propertyService = new PropertyService();
        user = new User();
        user.setLogin("test");
        final String password = "test";
        user.setPasswordHash(salt(propertyService, password));
        userRepository =
                new UserRepository(RootUserConst.ROOT_LOGIN, salt(propertyService, RootUserConst.ROOT_PASSWORD));
        userRepository.add(user);
        userService = new UserService(userRepository, propertyService);
    }

    @Test
    public void testOpenClose() {
        SessionService sessionService = new SessionService(sessionRepository, serviceLocator);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        Assert.assertNotNull(sessionService.open("test","test"));
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        Assert.assertNotNull(sessionService.findSessionByLogin("test"));
        session = sessionService.findSessionByLogin("test");
        Assert.assertTrue(sessionService.isValid(session));
        Assert.assertEquals(user.getId(),session.getUserId());
        sessionService.close(session);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void testCloseByLogin() {
        SessionService sessionService = new SessionService(sessionRepository, serviceLocator);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        Assert.assertNotNull(sessionService.open("test","test"));
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        Assert.assertNotNull(sessionService.findSessionByLogin("test"));
        session = sessionService.findSessionByLogin("test");
        Assert.assertTrue(sessionService.isValid(session));
        Assert.assertEquals(user.getId(),session.getUserId());
        sessionService.closeSessionByLogin("test","test");
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void testRootSession() {
        SessionService sessionService = new SessionService(sessionRepository, serviceLocator);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        Assert.assertNotNull(sessionService.open("root","root"));
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        Assert.assertNotNull(sessionService.findSessionByLogin("root"));
        session = sessionService.findSessionByLogin("root");
        Assert.assertTrue(sessionService.isValid(session));
        Assert.assertEquals(userRepository.findUserByLogin("root").getId(),session.getUserId());
        sessionService.closeSessionByLogin("root","root");
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }
}
