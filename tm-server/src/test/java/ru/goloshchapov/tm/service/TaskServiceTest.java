package ru.goloshchapov.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.repository.TaskRepository;

public class TaskServiceTest {


    private User user;
    private TaskRepository taskRepository;
    private TaskService taskService;
    private Task task;


    @Before
    public void before() {
        user = new User();
        taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);
        task = new Task();
    }

    @Test
    public void testStartFinish() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setUserId(user.getId());
        Assert.assertEquals(user.getId(), task.getUserId());
        Assert.assertNotNull(taskRepository.add(task));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertNotNull(taskService.startOneById(user.getId(), task.getId()));
        Assert.assertEquals(Status.IN_PROGRESS,task.getStatus());
        Assert.assertNotNull(taskService.finishOneById(user.getId(), task.getId()));
        Assert.assertEquals(Status.COMPLETE,task.getStatus());
    }

    @Test
    public void testAddUpdate() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        Assert.assertNotNull(taskService.add(user.getId(),"TASK","DESCRIPTION"));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertFalse(taskService.findAllByUserId(user.getId()).isEmpty());
        Assert.assertEquals("DESCRIPTION", taskService.findOneByName("TASK").getDescription());
        Assert.assertNotNull(taskService.findOneByName("TASK"));
        final Task project1 = taskService.findOneByName("TASK");
        Assert.assertNotNull(
                taskService.updateOneById(user.getId(), project1.getId(), "NAME", "DESC")
        );
        Assert.assertEquals("NAME", taskService.findOneById(project1.getId()).getName());
        Assert.assertEquals("DESC", taskService.findOneById(project1.getId()).getDescription());
    }

    @Test
    public void testChangeStatus() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setUserId(user.getId());
        Assert.assertEquals(user.getId(), task.getUserId());
        Assert.assertNotNull(taskRepository.add(task));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertNotNull(
                taskService.changeOneStatusByName(user.getId(),"TASK", "COMPLETE")
        );
        Assert.assertEquals(Status.COMPLETE, taskService.findOneByName("TASK").getStatus());
        Assert.assertNotNull(
                taskService.changeOneStatusById(user.getId(),task.getId(), "IN_PROGRESS")
        );
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findOneById(task.getId()).getStatus());
    }

}
