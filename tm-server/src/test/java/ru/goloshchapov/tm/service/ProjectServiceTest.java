package ru.goloshchapov.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.repository.ProjectRepository;


public class ProjectServiceTest {

    private User user;
    private ProjectRepository projectRepository;
    private ProjectService projectService;
    private Project project;


    @Before
    public void before() {
        user = new User();
        projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);
        project = new Project();
    }

    @Test
    public void testStartFinish() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setName("PROJECT");
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("PROJECT", project.getName());
        project.setUserId(user.getId());
        Assert.assertEquals(user.getId(), project.getUserId());
        Assert.assertNotNull(projectRepository.add(project));
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertNotNull(projectService.startOneById(user.getId(), project.getId()));
        Assert.assertEquals(Status.IN_PROGRESS,project.getStatus());
        Assert.assertNotNull(projectService.finishOneById(user.getId(), project.getId()));
        Assert.assertEquals(Status.COMPLETE,project.getStatus());
    }

    @Test
    public void testAddUpdate() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        Assert.assertNotNull(projectService.add(user.getId(),"PROJECT","DESCRIPTION"));
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertFalse(projectService.findAllByUserId(user.getId()).isEmpty());
        Assert.assertEquals("DESCRIPTION", projectService.findOneByName("PROJECT").getDescription());
        Assert.assertNotNull(projectService.findOneByName("PROJECT"));
        final Project project1 = projectService.findOneByName("PROJECT");
        Assert.assertNotNull(
                projectService.updateOneById(user.getId(), project1.getId(), "NAME", "DESC")
        );
        Assert.assertEquals("NAME", projectService.findOneById(project1.getId()).getName());
        Assert.assertEquals("DESC", projectService.findOneById(project1.getId()).getDescription());
    }

    @Test
    public void testChangeStatus() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setName("PROJECT");
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("PROJECT", project.getName());
        project.setUserId(user.getId());
        Assert.assertEquals(user.getId(), project.getUserId());
        Assert.assertNotNull(projectRepository.add(project));
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertNotNull(
                projectService.changeOneStatusByName(user.getId(),"PROJECT", "COMPLETE")
        );
        Assert.assertEquals(Status.COMPLETE, projectService.findOneByName("PROJECT").getStatus());
        Assert.assertNotNull(
                projectService.changeOneStatusById(user.getId(),project.getId(), "IN_PROGRESS")
        );
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findOneById(project.getId()).getStatus());
    }
}

