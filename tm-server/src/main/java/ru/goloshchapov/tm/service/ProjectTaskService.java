package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.api.service.IProjectTaskService;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Task;

import java.util.Date;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.checkIndex;
import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull private final ITaskRepository taskRepository;

    @NotNull private final IProjectRepository projectRepository;

    @NotNull
    public ProjectTaskService(
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IProjectRepository projectRepository
    ) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project addProject(
            @Nullable final String userId,
            @Nullable final String projectName,
            @Nullable final String projectDescription
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        projectRepository.add(userId, project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task addTask(
            @Nullable final String userId,
            @Nullable final String taskName,
            @Nullable final String taskDescription
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(taskName)) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    @SneakyThrows
    public void clearProjects(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        projectRepository.clear(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAllProjects(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return projectRepository.findAll(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (projectRepository.isAbsentById(projectId)) return null;
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public boolean isEmptyProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) return true;
        return findAllByProjectId(userId, projectId) == null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectName(@Nullable final String userId, @Nullable final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        if (projectRepository.isAbsentByName(projectName)) return null;
        @NotNull final String projectId = projectRepository.getIdByName(projectName);
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public boolean isEmptyProjectByName(@Nullable final String userId, @Nullable final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) return true;
        return findAllByProjectName(userId, projectName) == null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = projectRepository.size();
        if (!checkIndex(projectIndex, size)) throw new IndexIncorrectException();
        if (projectRepository.isAbsentByIndex(projectIndex)) return null;
        @NotNull String  projectId = projectRepository.getIdByIndex(projectIndex);
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public boolean isEmptyProjectByIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = projectRepository.size();
        if (!checkIndex(projectIndex, size)) return true;
        return findAllByProjectIndex(userId, projectIndex) == null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task bindToProjectById(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(taskId) || isEmpty(projectId)) throw new EmptyIdException();
        if (projectRepository.isAbsentById(projectId) || taskRepository.isAbsentById(taskId)) return null;
        return taskRepository.bindToProjectById(userId, taskId, projectId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task unbindFromProjectById(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(taskId) || isEmpty(projectId)) throw new EmptyIdException();
        if (projectRepository.isAbsentById(projectId) || taskRepository.isAbsentById(taskId)) return null;
        return taskRepository.unbindFromProjectById(userId, taskId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (projectRepository.isAbsentById(projectId)) return null;
        return taskRepository.removeAllByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> removeAllByProjectName(@Nullable final String userId, @Nullable final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        if (projectRepository.isAbsentByName(projectName)) return null;
        @NotNull final String projectId = projectRepository.getIdByName(projectName);
        return taskRepository.removeAllByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> removeAllByProjectIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = projectRepository.size();
        if (!checkIndex(projectIndex, size)) throw new IndexIncorrectException();
        if (projectRepository.isAbsentByIndex(projectIndex)) return null;
        @NotNull final String projectId = projectRepository.getIdByIndex(projectIndex);
        return taskRepository.removeAllByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public boolean removeAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @Nullable final List<Project> projects = projectRepository.findAllByUserId(userId);
        if (projects == null || projects.isEmpty()) return false;
        for (@NotNull final Project project: projects) {
            removeProjectById(userId, project.getId());
        }
        return true;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (projectRepository.isAbsentById(projectId)) return null;
        if (!isEmptyProjectById(userId, projectId)) removeAllByProjectId(userId, projectId);
        return projectRepository.removeOneById(userId, projectId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeProjectByName(@Nullable final String userId, @Nullable final String projectName) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(projectName)) throw new EmptyNameException();
        if (projectRepository.isAbsentByName(projectName)) return null;
        if (!isEmptyProjectByName(userId, projectName)) removeAllByProjectName(userId, projectName);
        return projectRepository.removeOneByName(userId, projectName);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeProjectByIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = projectRepository.size();
        if (!checkIndex(projectIndex, size)) throw new IndexIncorrectException();
        if (projectRepository.isAbsentByIndex(projectIndex)) return null;
        if (!isEmptyProjectByIndex(userId, projectIndex)) removeAllByProjectIndex(userId, projectIndex);
        return projectRepository.removeOneByIndex(userId, projectIndex);
    }

    @Override
    public void createTestData() {
        addProject("ddd","Project_1","About_1").setId("ppp111");
        addProject("ttt","Project_2","About_2").setId("ppp222");
        addProject("ttt","Project_3","About_3").setId("ppp333");
        addProject("ddd","Project_4","About_4").setId("ppp444");
        addProject("aaa","Project_5","About_5").setId("ppp555");
        @NotNull Date date = new Date();
        long time = date.getTime();
        projectRepository.findOneById("ppp111").setStatus(Status.COMPLETE);
        projectRepository.findOneById("ppp111").setDateStart(date);
        projectRepository.findOneById("ppp111").setDateFinish(new Date(time+300000));
        projectRepository.findOneById("ppp333").setStatus(Status.IN_PROGRESS);
        projectRepository.findOneById("ppp333").setDateStart(new Date(time+30000));
        projectRepository.findOneById("ppp555").setStatus(Status.IN_PROGRESS);
        projectRepository.findOneById("ppp555").setDateStart(new Date(time+40000));
        addTask("ddd","Task_1","Desc_1").setId("11111");
        addTask("ddd","Task_2","Desc_2").setId("22222");
        addTask("ttt","Task_3","Desc_3").setId("33333");
        addTask("ttt","Task_4","Desc_4").setId("44444");
        addTask("ttt","Task_5","Desc_5").setId("55555");
        addTask("ttt","Task_6","Desc_6").setId("66666");
        addTask("ddd","Task_7","Desc_7").setId("77777");
        addTask("ddd","Task_8","Desc_8").setId("88888");
        addTask("aaa","Task_9","Desc_9").setId("99999");
        addTask("aaa","Task_0","Desc_0").setId("00000");
        date = new Date();
        time = date.getTime();
        bindToProjectById("ddd","11111","ppp111").setStatus(Status.IN_PROGRESS);
        taskRepository.findOneById("11111").setDateStart(new Date(time + 60000));
        bindToProjectById("ddd","22222","ppp111").setStatus(Status.NOT_STARTED);
        bindToProjectById("ttt","33333","ppp222").setStatus(Status.COMPLETE);
        taskRepository.findOneById("33333").setDateStart(new Date());
        taskRepository.findOneById("33333").setDateFinish(new Date(time + 60000));
        bindToProjectById("ttt","44444","ppp222").setStatus(Status.IN_PROGRESS);
        taskRepository.findOneById("44444").setDateStart(new Date(time + 120000));
        bindToProjectById("ttt","55555","ppp333").setStatus(Status.NOT_STARTED);
        bindToProjectById("ttt","66666","ppp333").setStatus(Status.COMPLETE);
        taskRepository.findOneById("66666").setDateStart(new Date(time + 30000));
        taskRepository.findOneById("66666").setDateFinish(new Date(time + 270000));
        bindToProjectById("ddd","77777","ppp444").setStatus(Status.COMPLETE);
        taskRepository.findOneById("77777").setDateStart(new Date(time + 300000));
        taskRepository.findOneById("77777").setDateFinish(new Date(time + 1200000));
        bindToProjectById("ddd","88888","ppp444").setStatus(Status.IN_PROGRESS);
        taskRepository.findOneById("88888").setDateStart(new Date(time + 3000));
        bindToProjectById("aaa","99999","ppp555").setStatus(Status.NOT_STARTED);
        bindToProjectById("aaa","00000","ppp555").setStatus(Status.IN_PROGRESS);
        taskRepository.findOneById("00000").setDateStart(new Date(time + 330000));
    }

}
