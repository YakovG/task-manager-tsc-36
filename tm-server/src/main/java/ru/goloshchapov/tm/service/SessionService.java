package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.repository.ISessionRepository;
import ru.goloshchapov.tm.api.service.ISessionService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.util.SignatureUtil;

import java.util.List;

import static ru.goloshchapov.tm.util.HashUtil.salt;
import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull private final ISessionRepository sessionRepository;

    @Nullable private final ServiceLocator serviceLocator;

    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @Nullable final ServiceLocator serviceLocator
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> getListSession(final Session session) {
        validate(session);
        return sessionRepository.findAllSession();
    }

    @Nullable
    @Override
    public Session findSessionByLogin(@Nullable final String login) {
        @NotNull final User user = serviceLocator.getUserService().findUserByLogin(login);
        @Nullable final Session session = sessionRepository.findByUserId(user.getId());
        return session;
    }

    @Override
    public void closeSessionByLogin(@Nullable final String login, @Nullable final String password) {
        if (!checkDataAccess(login, password)) throw new AccessDeniedException();
        @Nullable final Session session = findSessionByLogin(login);
        close(session);
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login) || isEmpty(password)) return false;
        @NotNull final User user = serviceLocator.getUserService().findUserByLogin(login);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @Nullable final String passwordHash = salt(propertyService, password);
        if (isEmpty(passwordHash)) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @NotNull final User user = serviceLocator.getUserService().findUserByLogin(login);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.merge(session);
        return sign(session);
    }

    @Override
    public void close(@Nullable Session session) {
        validate(session);
        sessionRepository.exclude(session);
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findUserById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }
}
