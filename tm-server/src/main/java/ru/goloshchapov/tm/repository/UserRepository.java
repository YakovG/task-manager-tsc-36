package ru.goloshchapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.constant.RootUserConst;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;

import java.util.Optional;
import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private final User rootUser = new User();

    public Predicate<User> predicateByLogin (@NotNull final String login) {
        return u-> login.equals(u.getLogin());
    }

    public Predicate<User> predicateByEmail (@NotNull final String email) {
        return u -> email.equals(u.getEmail());
    }

    public UserRepository(@NotNull final String rootLogin, @Nullable String rootPassword) {
        rootUser.setLogin(rootLogin);
        rootUser.setPasswordHash(rootPassword);
        rootUser.setRole(Role.ADMIN);
    }

    @Nullable
    @Override
    public User findUserById(@NotNull final String id) {
        if (id.equals(rootUser.getId())) return rootUser;
        return findOneById(id);
    }

    @Nullable
    @Override
    public User findUserByLogin(@NotNull final String login) {
        if (RootUserConst.ROOT_LOGIN.equals(login)) return rootUser;
        return list.stream()
                .filter(predicateByLogin(login))
                .limit(1).findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findUserByEmail(@NotNull final String email) {
        return list.stream().
                filter(predicateByEmail(email)).
                limit(1).findFirst()
                .orElse(null);
    }

    @Override
    public final boolean isLoginExists(@NotNull final String login) {
        return findUserByLogin(login) != null;
    }

    @Override
    public final boolean isEmailExists(@NotNull final String email) {
        return findUserByEmail(email) != null;
    }

    @NotNull
    @Override
    public User removeUser(@NotNull final User user) {
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeUserByLogin(@NotNull final String login) {
        final Optional<User> user = Optional.ofNullable(findUserByLogin(login));
        user.ifPresent(list::remove);
        return user.orElse(null);
    }

    @Nullable
    @Override
    public User removeUserByEmail(@NotNull final String email) {
        final Optional<User> user = Optional.ofNullable(findUserByEmail(email));
        user.ifPresent(list::remove);
        return user.orElse(null);
    }

    @Nullable
    @Override
    public User lockUserByLogin(@NotNull final String login) {
        final Optional<User> user = Optional.ofNullable(findUserByLogin(login));
        user.ifPresent(u-> u.setLocked(true));
        return user.orElse(null);
    }

    @Nullable
    @Override
    public User unlockUserByLogin(@NotNull final String login) {
        final Optional<User> user = Optional.ofNullable(findUserByLogin(login));
        user.ifPresent(u-> u.setLocked(false));
        return user.orElse(null);
    }

    @Nullable
    @Override
    public User lockUserByEmail(@NotNull final String email) {
        final Optional<User> user = Optional.ofNullable(findUserByEmail(email));
        user.ifPresent(u-> u.setLocked(true));
        return user.orElse(null);
    }

    @Nullable
    @Override
    public User unlockUserByEmail(@NotNull final String email) {
        final Optional<User> user = Optional.ofNullable(findUserByEmail(email));
        user.ifPresent(u-> u.setLocked(false));
        return user.orElse(null);
    }

}
