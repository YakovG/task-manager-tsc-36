package ru.goloshchapov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.endpoint.*;
import ru.goloshchapov.tm.api.repository.*;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.constant.RootUserConst;
import ru.goloshchapov.tm.endpoint.*;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.repository.*;
import ru.goloshchapov.tm.service.*;
import ru.goloshchapov.tm.util.SystemUtil;
import ru.goloshchapov.tm.util.TerminalUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Predicate;

import static ru.goloshchapov.tm.util.HashUtil.salt;

public final class Bootstrap implements ServiceLocator {

    @NotNull private final ServiceLocator serviceLocator = this;

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IDataService dataService = new DataService(serviceLocator);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository =
            new UserRepository(RootUserConst.ROOT_LOGIN, salt(propertyService, RootUserConst.ROOT_PASSWORD));

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @Nullable
    private final ISessionService sessionService = new SessionService(sessionRepository, serviceLocator);

    @Getter
    @NotNull
    private final ICalculatorEndpoint calculatorEndpoint = new CalculatorEndpoint();

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @Getter
    @NotNull
    private final ITaskProjectEndpoint taskProjectEndpoint = new TaskProjectEndpoint(serviceLocator);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(serviceLocator);

    @Getter
    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(serviceLocator);

    private void checkPreliminary() {
        initPID();
        final Predicate<String> checkDefault = "y"::equals;
        System.out.println("CREATE DEFAULT USERS? (y)");
        if (checkDefault.test(TerminalUtil.nextLine())) {
            serviceLocator.getUserService().create("admin","admin", Role.ADMIN).setId("aaa");
            serviceLocator.getUserService().create("test","test","test@test.tt").setId("ttt");
            serviceLocator.getUserService().create("demo","demo", "demo@demo.dd").setId("ddd");
            loggerService.info("DEFAULT USERS CREATED");
        }
        System.out.println("USE TEST DATASET? (y)");
        if (checkDefault.test(TerminalUtil.nextLine())) {
            serviceLocator.getProjectTaskService().createTestData();
            loggerService.info("TEST DATASET CREATED");
        }
    }

    private void initEndpoint(@NotNull final String url, @NotNull final Object endpoint) {
        System.out.println(url);
        Endpoint.publish(url, endpoint);
    }

    private void promulgate(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        initEndpoint(wsdl, endpoint);
    }

    public void run() {
        loggerService.debug("DEBUG INFO!");
        initEndpoint();
        checkPreliminary();
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void initEndpoint() {
        promulgate(calculatorEndpoint);
        promulgate(projectEndpoint);
        promulgate(sessionEndpoint);
        promulgate(taskEndpoint);
        promulgate(taskProjectEndpoint);
        promulgate(userEndpoint);
        promulgate(adminEndpoint);
    }
}
