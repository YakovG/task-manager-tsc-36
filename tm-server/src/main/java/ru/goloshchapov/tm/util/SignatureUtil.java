package ru.goloshchapov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface SignatureUtil {

    @Nullable
    static String sign(
            @Nullable final Object value,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) {
        try
        {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        }
        catch (@NotNull final JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    static String sign(
            @Nullable final String value,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) {
        if (value == null || salt == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.md5(salt + result + salt);
        }
        return result;
    }
}
