package ru.goloshchapov.tm.model;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.entity.IWBS;
import ru.goloshchapov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    @Nullable
    private String userId;

    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Date created = new Date();

    @NotNull
    public AbstractBusinessEntity(@NotNull final String userId) {
        this.userId = userId;
    }

    public boolean checkUserAccess(@NotNull final String userId) {
        return userId.equals(getUserId());
    }

    @NotNull
    @Override
    public String toString() { return getId() + ". " + name; }

}
