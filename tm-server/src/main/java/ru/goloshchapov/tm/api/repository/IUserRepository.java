package ru.goloshchapov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IRepository;
import ru.goloshchapov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findUserById(@NotNull String id);

    @Nullable
    User findUserByLogin(String login);

    @Nullable
    User findUserByEmail(String email);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    @NotNull
    User removeUser(User user);

    @Nullable
    User removeUserByLogin(String login);

    @Nullable
    User removeUserByEmail(String email);

    @Nullable
    User lockUserByLogin(String login);

    @Nullable
    User unlockUserByLogin(String login);

    @Nullable
    User lockUserByEmail(String email);

    @Nullable
    User unlockUserByEmail(String email);
}
