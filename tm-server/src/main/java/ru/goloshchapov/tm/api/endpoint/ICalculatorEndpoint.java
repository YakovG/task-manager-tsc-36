package ru.goloshchapov.tm.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ICalculatorEndpoint {
    @WebMethod
    int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    );
}
