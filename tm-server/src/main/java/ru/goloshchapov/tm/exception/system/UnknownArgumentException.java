package ru.goloshchapov.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(@Nullable final String value) {
        super("Error! Undetermined argument " + value + " ...");
    }

}
